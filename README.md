# Docker Module Task - Generation K - VII of Kairós DS

# This task involved:

- Cloning the instructor's repository with projects already implemented.
- Creating a Dockerfile for each project.
- Creating a docker-compose file to orchestrate the containers and using a .env file to store variables.
- The .env.example file was created only for didactic purpose.

## To start the containers

```
docker-compose up
```
